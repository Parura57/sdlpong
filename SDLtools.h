#include<SDL2/SDL.h>
#include<math.h>

#define PI 3.14159

// Less efficient implementation of the Midpoint circle algorithm, basically find every point that aproach x^2 = r^2 - y^2
int SDLT_RenderCircle(SDL_Renderer *renderer, int x, int y, int radius) {
    for (int i = 0; i < radius; i++) {
        //SDL_RenderDrawPoint(renderer, x + i, y + floor(sqrt(radius * radius - i * i)));
        SDL_Point firstCoordinates = {i, static_cast<int>(floor(sqrt(radius * radius - i * i))) + 1 };

        SDL_Point coordinates[8] = {
        {x+firstCoordinates.x, y+firstCoordinates.y},
        {x-firstCoordinates.x, y+firstCoordinates.y},
        {x-firstCoordinates.x, y-firstCoordinates.y},
        {x+firstCoordinates.x, y-firstCoordinates.y},
        {x+firstCoordinates.y, y+firstCoordinates.x},
        {x-firstCoordinates.y, y+firstCoordinates.x},
        {x-firstCoordinates.y, y-firstCoordinates.x},
        {x+firstCoordinates.y, y-firstCoordinates.x},
        };

        SDL_RenderDrawPoints(renderer, coordinates, 8);
    }
    return 0;
}

int SDLT_RenderFullCircle(SDL_Renderer *renderer, int x, int y, int radius) {
    for (int i = 0; i < radius; i++) {
        //SDL_RenderDrawPoint(renderer, x + i, y + floor(sqrt(radius * radius - i * i)));
        SDL_Point firstCoordinates = {i, static_cast<int>(floor(sqrt(radius * radius - i * i))) + 1 };

        SDL_RenderDrawLine(renderer, x+firstCoordinates.x, y+firstCoordinates.y, x-firstCoordinates.x, y+firstCoordinates.y);
        SDL_RenderDrawLine(renderer, x+firstCoordinates.x, y-firstCoordinates.y, x-firstCoordinates.x, y-firstCoordinates.y);
        SDL_RenderDrawLine(renderer, x+firstCoordinates.y, y+firstCoordinates.x, x-firstCoordinates.y, y+firstCoordinates.x);
        SDL_RenderDrawLine(renderer, x+firstCoordinates.y, y-firstCoordinates.x, x-firstCoordinates.y, y-firstCoordinates.x);
        SDL_RenderDrawLine(renderer, x, y, x+radius, y);
        SDL_RenderDrawLine(renderer, x, y, x, y+radius);
    }
    return 0;
}
