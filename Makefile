UNAME=$(shell uname)

CC=/usr/bin/g++
INCLUDE=$(shell pkg-config --cflags --libs sdl2)

# idk, it breaks on my mac with different options than these
ifeq ($(UNAME), Darwin)
	CC=/opt/homebrew/bin/c++-12
	INCLUDE=-D_THREAD_SAFE -I/opt/homebrew/include -I/opt/homebrew/include/SDL2 -L/opt/homebrew/lib -lSDL2
endif

build:
	${CC} main.cpp ${INCLUDE} -O2 -o sdltest

run: build
	./sdltest
