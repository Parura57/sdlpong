#include<SDL2/SDL.h>
#include<stdio.h>
#include<unistd.h>
#include<math.h>

#include"game.h"
#include"SDLtools.h"

int Game::Init() {
    // Initialize window
   if (SDL_Init(SDL_INIT_VIDEO) < 0) 
        printf("SDL could not initialize, Error: %s \n", SDL_GetError());

    w = SDL_CreateWindow("SDLpong", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenW, screenH, SDL_WINDOW_SHOWN);
    if (w == NULL)
        printf("Window could not be created, Error: %s \n", SDL_GetError());

    render = SDL_CreateRenderer(w, -1, 0);
    if (render == NULL)
        printf("Renderer could not be created, Error: %s \n", SDL_GetError());

    // Initialize player
    player = new Player;

    // Initialize ball
    ball = new Ball;

    printf("%f\n", PI);
    // Initialize bricks
    for (int i = 0; i < brickNum; i++) {
            bricks[i].x = 50 + (i % 10) * (bricks[i].width + 5);
            bricks[i].y = 80 + (i % 3) * (bricks[i].width + 5);
            //printf("brick %i x%f y%f w%i h%i\n", i, bricks[i].x, bricks[i].y, bricks[i].width, bricks[i].height);
    }

    return 0;
}

int Game::EventHandler(SDL_Event e) {
    switch(e.type) {
    case SDL_QUIT: 
        gameRunning = false;
        break;

    case SDL_KEYDOWN:
        if (e.key.keysym.scancode == SDL_SCANCODE_LEFT)
            player->direction = -1;
        else if (e.key.keysym.scancode == SDL_SCANCODE_RIGHT)
            player->direction = 1;
        break;

    case SDL_KEYUP:
        if ((e.key.keysym.scancode == SDL_SCANCODE_LEFT && player->direction == -1)||
            (e.key.keysym.scancode == SDL_SCANCODE_RIGHT && player->direction == 1))
            player->direction = 0;
        break;
    }
    return 0;
}

int Game::Update() {
    if (score == brickNum) {
        printf("YOU WIN!");
        gameRunning = false;
    }

    // Move player
    if ((player->x <= 0 && player->direction == -1) ||
        (player->x + player->width >= screenW && player->direction == 1)) goto dontmoveplayer;
    player->x += player->speed * player->direction;
    dontmoveplayer:

    // Ball collision
    // Walls
    if (ball->x - ball->size <= 0) ball->xVelocity = - ball->xVelocity;
    else if (ball->x + ball->size >= screenW) ball->xVelocity = -ball->xVelocity;
    if (ball->y - ball->size <= 0) ball->yVelocity = -ball->yVelocity;
    else if (ball->y + ball->size >= screenH) gameRunning = false;
    // Player
    if (ball->y + ball->size >= player->y &&
        ball->y - ball->size <= player->y + player->height &&
        ball->x >= player->x &&
        ball->x <= player->x + player->width) {
            //printf("player collision\n");
            ball->yVelocity = -ball->yVelocity;
            ball->y = player->y - ball->size;       // Failsafe for clipping into the platform
    }
    // Bricks
    for (int i = 0; i < brickNum; i++) {
        if (!bricks[i].active) continue;
        float intersection;
        // Check top edge detection by solving for the intersection between the equation for the ball and that for the player
        intersection = sqrt(pow(static_cast<float>(ball->size), 2.0) - pow(bricks[i].y - ball->y, 2)) - ball->x;
        // NaN comparisons are always false, checking for an intersection
        if (intersection == intersection) {
            if (ball->x + ball->size >= bricks[i].x &&
                ball->x - ball->size <= bricks[i].x + bricks[i].width) {
                    //printf("hitting brick from the top%i\n", i);
                    bricks[i].active = false;
                    if (ball->x < bricks[i].x && ball->y <= bricks[i].y + ball->size) {    // Hitting from the left
                        ball->xVelocity = -ball->xVelocity;
                    }
                    else if (ball->x > bricks[i].x + bricks[i].width && ball->y <= bricks[i].y + ball->size) {    // Hitting from the right
                        ball->xVelocity = -ball->xVelocity;
                    }
                    else {    // Hitting straight from the top
                        ball->yVelocity = -ball->yVelocity;
                    }
            score += 1;
            break;
            }
            continue;
        }

        // Check bottom edge
        intersection = sqrt(pow(static_cast<float>(ball->size), 2.0) - pow(bricks[i].y + bricks[i].height - ball->y, 2)) - ball->x;
        if (intersection == intersection) {
            if (ball->x + ball->size >= bricks[i].x &&
                ball->x - ball->size <= bricks[i].x + bricks[i].width) {
                    //printf("hitting brick from the bottom%i\n", i);
                    bricks[i].active = false;
                    if (ball->x < bricks[i].x && ball->y <= bricks[i].y + ball->size) {    // Hitting from the left
                        ball->xVelocity = -ball->xVelocity;
                    }
                    else if (ball->x > bricks[i].x + bricks[i].width && ball->y <= bricks[i].y + ball->size) {    // Hitting from the right
                        ball->xVelocity = -ball->xVelocity;
                    }
                    else {    // Hitting straight from the bottom
                        ball->yVelocity = -ball->yVelocity;
                    }
            score += 1;
            break;
            }
        }
    }

    // Move ball
    ball->x += ball->xVelocity;
    ball->y += ball->yVelocity;

    return 0;
}

int Game::Render() {
    // Clear
    SDL_SetRenderDrawColor(render, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(render);

    // Player
    SDL_Rect playerRect = { static_cast<int>(player->x), static_cast<int>(player->y), player->width, player->height };
    SDL_SetRenderDrawColor(render, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderFillRect(render, &playerRect);

    // Ball
    SDL_SetRenderDrawColor(render, 0x00, 0x00, 0xFF, 0xFF);
    SDLT_RenderFullCircle(render, ball->x, ball->y, ball->size);

    // Bricks
    SDL_Rect brickRect;
    SDL_SetRenderDrawColor(render, 0xBB, 0x22, 0x00, 0xFF);
    for (int i = 0; i < brickNum; i++) {
        if (!bricks[i].active) continue;
        brickRect = { static_cast<int>(bricks[i].x), static_cast<int>(bricks[i].y), bricks[i].width, bricks[i].height };
        SDL_RenderFillRect(render, &brickRect);
    }

    SDL_RenderPresent(render);
    return 0;
}

int main() {
    Game game;
    game.Init();
    printf("initialised\n");

    // Main loop
    SDL_Event event;
    SDL_PumpEvents();
    while (game.gameRunning) {
        while (SDL_PollEvent(&event)) {
            game.EventHandler(event);
            SDL_PumpEvents();
        }
        game.Update();
        game.Render();
        //printf("frame\n");
        usleep(16*1000);   // In microseconds
    }

    // Exit
    SDL_DestroyWindow(game.w);
    SDL_Quit();
    return 0;
}
