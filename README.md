# SDLpong

A pong clone written in C++ with SDL2

The Makefile's include and library paths are configured for a Mac with SDL installed through homebrew, it might need reconfiguring for other systems (such as with pkg-config).

To run the game, clone the repository and run 

    $ make run

