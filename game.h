#include<stdlib.h>
#include<time.h>
const int screenW = 640;
const int screenH = 480;
const int brickNum = 30;

class Game {
public:
    Game() {
        w = NULL;
        render = NULL;
        gameRunning = true;
        score = 0;
    }
    SDL_Window *w;
    SDL_Renderer *render;
    bool gameRunning;
    int score;

    int Init();
    int EventHandler(SDL_Event e);
    int Update();
    int Render();

private:
    class Player;
    Player *player;

    class Ball;
    Ball *ball;

    class Brick {     // Has to be defined here for the array thing to work, idk
    public:
        Brick() {
            active = true;
            width = 50;
            height = 30;
        }
        bool active;
        int width, height;
        float x,y;
    };

    Brick bricks[brickNum];
};

class Game::Player {
public:
    Player() {
        width = 100;
        height = 20;
        x = screenW / 2 - width/2;
        y = screenH - 80 - height/2;
        speed = 10;
        direction = 0;
    }
    int width, height;
    float x,y;

    float speed;
    signed char direction;
};

class Game::Ball {
public:
    Ball() {
        size = 10;
        x = screenW / 2;
        y = screenH / 2 + 100;
        srand(time(NULL));
        xVelocity = rand() % 5 + 2;
        yVelocity = - (rand() % 5 + 2);
    }
    int size;
    float x,y;

    float xVelocity, yVelocity;
};


